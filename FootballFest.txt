#include <iostream>
using namespace std;

int main() 
{
	//variable declaration
	int testCase , numberOfPasses , previousId , currentId;

    	//taking input of number of test cases.
	cin>>testCase;

    	while(testCase--) 
	{
   	 	
		 //taking the input of toatl number of passes and id to start with from users
    		cin>>numberOfPasses;
		cin>>previousId;
		
    		currentId = previousId;

    		while(numberOfPasses--)
		{
        		//selecting the type of pass
			char typeOfPass;
	      		cin>>typeOfPass; 

               		if(typeOfPass == 'P')
			{
            			//id to get the next pass is taken by user
				int userInputId;
            			cin>>userInputId;
            			previousId = currentId;
            			currentId = userInputId;
        		}
        		else
			{
               			int swapTemp;
				swapTemp = previousId;
            			previousId = currentId;
            			currentId = swapTemp;
        		}
   		 }
    		cout<<"Player "<<currentId<<endl;
    	}
    	return 0;
}
