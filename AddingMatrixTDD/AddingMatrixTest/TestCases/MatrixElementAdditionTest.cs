﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AddingMatrixTDD.Services;

namespace AddingMatrixTest
{
    [TestClass]
    public class MatrixElementAdditionTest
    {
        MatrixElementsAddition matrixElementsAddition = new MatrixElementsAddition();
        public Tuple<int[], int[]> selectedElementsArray;

        //Naming convention: NameofMethodUnderTest_StateUnderTest_ExpectedBehaviour

        [TestMethod]
        public void SelectElementsToAdd_LoadValidMatrix_ReturnEvenElementsArray()
        {
            //Arrange
            int[ , ] inputMatrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            //Act          
            selectedElementsArray = matrixElementsAddition.SelectElementsToAdd(inputMatrix);

            //Asert
            int[] evenElements = { 2, 4, 6, 8 };
            Assert.Equals(evenElements, selectedElementsArray.Item1);
        }

        [TestMethod]
        public void SelectElementsToAdd_LoadValidMatrix_ReturnOddElementsArray()
        {
            //Arrange
            int[,] inputMatrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            //Act          
            selectedElementsArray = matrixElementsAddition.SelectElementsToAdd(inputMatrix);

            //Asert
            int[] oddElements = { 1, 3, 5, 7, 9 };
            Assert.Equals(oddElements, selectedElementsArray.Item2);
        }

        public void AddSelectedElements_LoadElementstoAdd_ReturnArrayofSum()
        {
            //Arrange
            int[] evenElements = { 2, 4, 6, 8 };
            int[] oddElements = { 1, 3, 5, 7, 9 };
            Tuple<int[], int[]> selectedElementstoAdd = new Tuple<int[], int[]>(evenElements, oddElements);

            //Act          
            int[] sum = matrixElementsAddition.AddSelectedElements(selectedElementstoAdd);

            //Asert
            int[] expectedSum = { 20, 25 };
            Assert.Equals(expectedSum, sum);
        }
    }
}
