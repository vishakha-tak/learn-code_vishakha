﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AddingMatrixTDD.Services;
using System.Linq;

namespace AddingMatrixTest.TestCases
{
    [TestClass]
    public class CreationOfMatrixTest
    {
        MatrixElementsAddition matrixElementsAddition = new MatrixElementsAddition();

        //Naming convention: NameofMethodUnderTest_StateUnderTest_ExpectedBehaviour

        [TestMethod]
        public void GetUserInput_LoadValidInput_ReturnValidArray()
        {
            //Arrange
            string userInput = "1 2 3 4 5 6 7 8 9";
            int[] expectedInputArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
           
            //Act            
            int[] userInputArray = matrixElementsAddition.GetUserInput(userInput);

            //Assert
            Assert.Equals(expectedInputArray, userInputArray);

        }

        [TestMethod]
        public void GetUserInput_LoadInValidDatatypeInput_ThrowFormatException()
        {
            try
            {
                //Arrange
                string userInput = "Test 2 3 4 5 6 7 8 9";
                int[] expectedInputArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

                //Act            
                int[] userInputArray = matrixElementsAddition.GetUserInput(userInput);

                //Assert
                Assert.Fail();
            }
            catch(FormatException ex)
            {
                //the test passes
            }

        }

        [TestMethod]
        public void CreateMatrix_LoadValidDataInArray_ReturnValidMatrix()
        {
            //Arrange
            string userInput = "1 2 3 4 5 6 7 8 9";
            int[] inputArray = userInput.Select(str => int.Parse(userInput)).ToArray();

            //Act            
            int[,] createdMatrix = matrixElementsAddition.CreateMatrix(inputArray);

            //Assert
            Assert.Equals(1, createdMatrix[0,0]);
            Assert.Equals(2, createdMatrix[0,1]);
            Assert.Equals(3, createdMatrix[0,2]);
            Assert.Equals(4, createdMatrix[1,0]);
            Assert.Equals(5, createdMatrix[1,1]);
            Assert.Equals(6, createdMatrix[1,2]);
            Assert.Equals(7, createdMatrix[2,0]);
            Assert.Equals(8, createdMatrix[2,1]);
            Assert.Equals(9, createdMatrix[2,2]);
        }

        [TestMethod]
        public void CreateMatrix_LoadNullArray_ThrowFormatException()
        {
            try
            {
                //Arrange
                int[] inputArray = { };

                //Act            
                int[,] createdMatrix = matrixElementsAddition.CreateMatrix(inputArray);

                //Assert
                Assert.Fail();
            }
            catch(ArgumentNullException ex)
            {
                //the test passes
            }           
        }

        [TestMethod]
        public void CreateMatrix_AccessInvalidIndex_ThrowIndexOutOfRangeException()
        {
            try
            {
                //Arrange
                string userInput = "1 2 3 4 5 6 7 8 9";
                int[] inputArray = userInput.Select(str => int.Parse(userInput)).ToArray();

                //Act            
                int[,] createdMatrix = matrixElementsAddition.CreateMatrix(inputArray);

                //Assert
                Assert.Equals(1,createdMatrix[1,7]);
            }
            catch(IndexOutOfRangeException ex)
            {
                //The test passes 
            }
        }
    }
}
