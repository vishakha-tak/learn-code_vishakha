﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddingMatrixTDD.Contracts
{
    public interface IAddingMatrixElements
    {
        int[] GetUserInput(string input);

        int[,] CreateMatrix(int[] arrayElements) ;

        Tuple<int[], int[]> SelectElementsToAdd(int[,] inputMatrix);

        int[] AddSelectedElements(Tuple<int[], int[]> elementsToAdd);

        void DisplayResult(int[] arrayOfSum);

    }
}
