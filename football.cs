using System;

namespace LearnAndCode
{
    public class Operations
    {
        public int currentId, previousId = 0;

        public bool InputValidations(int input, int minimumValue, int maximumValue)
        {
            if (minimumValue < input & input <= maximumValue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int InitialInput()
        {
            Console.WriteLine("enter the no. of passes and starting id");
            string input = Console.ReadLine();
            string[] split = input.Split(null);

            int numberOfPasses = Convert.ToInt32(split[0]);

            int previousId = Convert.ToInt32(split[1]);
            currentId = previousId;
            return numberOfPasses;
        }

        public void ForwardPass(int nextId)
        {
            previousId = currentId;
            currentId = nextId;
        }

        public void BackPass()
        {
            int swapTemp = previousId;
            previousId = currentId;
            currentId = swapTemp;
        }

    }

    class FootballProblem
    {

        const int maximumTestCase = 100;
        const int minimumTestCase = 0;
        const int minimumNumberOfPasses = 0;
        const int maximumNumberOfPasses = 100000;
        const int minimumId = 0;
        const int maximumId = 1000000;

        static void Main(string[] args)
        {
            Operations inputObj = new Operations();
            Console.WriteLine("enter no. of test cases");
            int testCase = Convert.ToInt32(Console.ReadLine());
            int numberOfPasses = inputObj.InitialInput();

            while (inputObj.InputValidations(testCase, minimumTestCase, maximumTestCase))
            {

                while (inputObj.InputValidations(numberOfPasses, minimumNumberOfPasses, maximumNumberOfPasses))
                {
                    numberOfPasses--;
                    Console.WriteLine("enter the type of pass");
                    string input2 = Console.ReadLine();
                    string[] split2 = input2.Split(' ');
                    string typeOfPass = Convert.ToString(split2[0]);

                    if (typeOfPass == "P")
                    {
                        Console.WriteLine("enter id to pass to");
                        inputObj.ForwardPass(int.Parse(split2[1]));
                    }
                    else
                    {
                        inputObj.BackPass();
                    }
                }

                testCase--;
                Console.WriteLine("Player {0}", inputObj.currentId);

                Console.ReadLine();
            }
        }

    }
}
