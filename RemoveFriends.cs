﻿using System;
using System.Linq;

namespace ChristieFacebookFriends
{
    class RemoveFriends
    {
        public FriendList head = null;
        public FriendList last = null;
        public static void Main(String[] args)
        {
            int noOfTestCases = Convert.ToInt32(Console.ReadLine());

            for (int count = 0; count < noOfTestCases; count++)
            {
                ChristieFriendList listOfFriend = new ChristieFriendList();

                int totalFriendsToBeDeleted = listOfFriend.FriendListInput();
                listOfFriend.RemoveFriends(totalFriendsToBeDeleted);
                listOfFriend.DisplayRemainingFriends();
            }
            Console.ReadLine();
        }
    }
    public class FriendList
    {
        private FriendList _nextFriend;
        private FriendList _prevFriend;
        private int _popularity;
        public FriendList(int popularity, FriendList next, FriendList prev)
        {
            _popularity = popularity;
            _nextFriend = next;
            _prevFriend = prev;
        }
        public FriendList()
        {
            _popularity = 0;
            _nextFriend = null;
            _prevFriend = null;
        }
        public int Popularity

        {
            get
            {
                return _popularity;
            }
            set
            {
                _popularity = value;
            }
        }
        public FriendList NextFriend
        {
            get
            {
                return _nextFriend;
            }
            set
            {
                _nextFriend = value;
            }
        }
        public FriendList PrevFriend
        {
            get
            {
                return _prevFriend;
            }
            set
            {
                _prevFriend = value;
            }
        }
    }

    public class ChristieFriendList
    {
        RemoveFriends removeFriends = new RemoveFriends();
        public int FriendListInput()
        {
            FriendList current = null;
            var numbers = Console.ReadLine().Trim().Split(' ').Select(int.Parse).ToArray();
            int totalFriends = numbers[0];
            int totalFriendsToBeDeleted = numbers[1];
            String[] line = Console.ReadLine().Split(' ');
            int[] popularities = Array.ConvertAll(line, int.Parse);

            foreach (var popularity in popularities)
            {
                if (removeFriends.head == null)
                {
                    removeFriends.head = new FriendList(popularity, current, null);
                    //head.Next = current;
                    removeFriends.last = removeFriends.head;
                }
                else
                {
                    removeFriends.last.NextFriend = new FriendList(popularity, null, null);
                    removeFriends.last.NextFriend.PrevFriend = removeFriends.last;
                    removeFriends.last = removeFriends.last.NextFriend;
                }
                current = removeFriends.head;
            }
            return totalFriendsToBeDeleted;
        }

        public void RemoveFriends(int totalFriendsToBeDeleted)
        {
            bool flag = false;
            FriendList friend = new FriendList();
            friend = removeFriends.head;
            while (totalFriendsToBeDeleted != 0)
            {
                if (friend.NextFriend == null)
                {
                    break;
                }
                else if ((friend.Popularity) < (friend.NextFriend.Popularity))
                {
                    if (friend.PrevFriend == null)
                    {
                        removeFriends.head = friend.NextFriend;
                        removeFriends.head.PrevFriend = null;
                        friend = friend.NextFriend;
                    }
                    else
                    {
                        friend.PrevFriend.NextFriend = friend.NextFriend;
                        friend.NextFriend.PrevFriend = friend.PrevFriend;
                        friend = friend.PrevFriend;
                    }
                    --totalFriendsToBeDeleted;
                    flag = true;
                }
                else
                {
                    friend = friend.NextFriend;
                }
            }
            if (flag == false)
            {
                removeFriends.last = removeFriends.last.PrevFriend;
                removeFriends.last.NextFriend = null;
            }
        }

        public void DisplayRemainingFriends()
        {
            //Node temp = new Node();
            //temp = friends.head;
            while (removeFriends.head != null)
            {
                Console.Write("{0} ", removeFriends.head.Popularity);
                removeFriends.head = removeFriends.head.NextFriend;
            }
            Console.Write("\n");
        }
    }
}