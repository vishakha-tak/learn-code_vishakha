﻿using System;
using Adding2DArray.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Adding2DArray.Test.TestCases
{
    [TestClass]
    public class ArrayOperationTest
    {
        ArrayOperations arrayOperations = new ArrayOperations();
        public Tuple<int[], int[]> selectedElementsArray;
        int[] indexToStart = { 0, 1 };

        //Naming convention: NameofMethodUnderTest_StateUnderTest_ExpectedBehaviour

        [TestMethod]
        public void SelectElementsToAdd_LoadValidArray_ReturnEvenElementsArray()
        {
            //Arrange
            int[,] inputArray = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            //Act          
            selectedElementsArray = arrayOperations.SelectElementsToAdd(inputArray, indexToStart);

            //Assert
            int[] evenElements = { 2, 4, 6, 8 };
            Assert.Equals(evenElements, selectedElementsArray.Item1);
        }

        [TestMethod]
        public void SelectElementsToAdd_LoadValidArray_ReturnOddElementsArray()
        {
            //Arrange
            int[,] inputArray = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            //Act          
            selectedElementsArray = arrayOperations.SelectElementsToAdd(inputArray, indexToStart);

            //Assert
            int[] oddElements = { 1, 3, 5, 7, 9 };
            Assert.Equals(oddElements, selectedElementsArray.Item2);
        }

        public void AddSelectedElements_LoadElementstoAdd_ReturnArrayofSum()
        {
            //Arrange
            int[] evenElements = { 2, 4, 6, 8 };
            int[] oddElements = { 1, 3, 5, 7, 9 };
            Tuple<int[], int[]> selectedElementstoAdd = new Tuple<int[], int[]>(evenElements, oddElements);

            //Act          
            int[] sum = arrayOperations.AddSelectedElements(selectedElementstoAdd);

            //Assert
            int[] expectedSum = { 20, 25 };
            Assert.Equals(expectedSum, sum);
        }
    }
}
