﻿using System;
using Adding2DArray.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Adding2DArray.Test.TestCases
{
    [TestClass]
    public class UserInputTest
    {
        FormatUserInput input = new FormatUserInput();
        //Naming convention: NameofMethodUnderTest_StateUnderTest_ExpectedBehaviour

        [TestMethod]
        public void GetUserInput_LoadValidInput_ReturnValidArray()
        {
            //Arrange
            string userInput = "1 2 3 4 5 6 7 8 9";
            int[] expectedInputArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            //Act            
            int[] userInputArray = input.FormatInput(userInput);

            //Assert
            Assert.Equals(expectedInputArray, userInputArray);

        }
        [TestMethod]
        public void GetUserInput_LoadInValidDatatypeInput_ThrowFormatException()
        {
            try
            {
                //Arrange
                string userInput = "Test 2 3 4 5 6 7 8 9";
                int[] expectedInputArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

                //Act            
                int[] userInputArray = input.FormatInput(userInput);

                //Assert
                Assert.Fail();
            }
            catch (FormatException ex)
            {
                //the test passes
            }

        }
    }
}
