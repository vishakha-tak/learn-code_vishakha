﻿using System;
using Adding2DArray.Services;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Adding2DArray.Test.TestCases
{
    [TestClass]
    public class ArrayCreationTest
    {
        ArrayOperations arrayOperations = new ArrayOperations();

        //Naming convention: NameofMethodUnderTest_StateUnderTest_ExpectedBehaviour      

        [TestMethod]
        public void Create2DArray_LoadValidDataInArray_ReturnValid2DArray()
        {
            //Arrange
            string userInput = "1 2 3 4 5 6 7 8 9";
            int[] inputArray = userInput.Select(str => int.Parse(userInput)).ToArray();

            //Act            
            int[,] createdArray = arrayOperations.Create2DArray(inputArray);

            //Assert
            Assert.Equals(1, createdArray[0, 0]);
            Assert.Equals(2, createdArray[0, 1]);
            Assert.Equals(3, createdArray[0, 2]);
            Assert.Equals(4, createdArray[1, 0]);
            Assert.Equals(5, createdArray[1, 1]);
            Assert.Equals(6, createdArray[1, 2]);
            Assert.Equals(7, createdArray[2, 0]);
            Assert.Equals(8, createdArray[2, 1]);
            Assert.Equals(9, createdArray[2, 2]);
        }

        [TestMethod]
        public void Create2DArray_LoadNullArray_ThrowFormatException()
        {
            try
            {
                //Arrange
                int[] inputArray = { };

                //Act            
                int[,] createdArray = arrayOperations.Create2DArray(inputArray);

                //Assert
                Assert.Fail();
            }
            catch(ArgumentNullException ex)
            {
                //the test passes
            }           
        }

        [TestMethod]
        public void Create2DArray_AccessInvalidIndex_ThrowIndexOutOfRangeException()
        {
            try
            {
                //Arrange
                string userInput = "1 2 3 4 5 6 7 8 9";
                int[] inputArray = userInput.Select(str => int.Parse(userInput)).ToArray();

                //Act            
                int[,] createdArray = arrayOperations.Create2DArray(inputArray);

                //Assert
                Assert.Equals(1, createdArray[1, 7]);
            }
            catch(IndexOutOfRangeException ex)
            {
                //The test passes 
            }
        }
    
    }
}
