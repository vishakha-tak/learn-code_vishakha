﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adding2DArray.Services
{
    public class Program
    {
        static void Main(string[] args)
        {
            FormatUserInput input = new FormatUserInput();
            ArrayOperations arrayOperation = new ArrayOperations();
            DisplayOutput displayOutput = new DisplayOutput();

            string userInput = input.GetUserInput();
            int[] inputArray = input.FormatInput(userInput);

            int[,] twoDArray = arrayOperation.Create2DArray(inputArray);

            int[] indexToStart = { };
            Tuple<int[], int[]> selectedElementsToAdd = arrayOperation.SelectElementsToAdd(twoDArray, indexToStart);
            int[] arrayofSum = arrayOperation.AddSelectedElements(selectedElementsToAdd);

            displayOutput.DisplayResult(arrayofSum);

        }
    }
}
