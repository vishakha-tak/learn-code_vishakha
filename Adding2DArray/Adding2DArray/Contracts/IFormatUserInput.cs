﻿using System;

namespace Adding2DArray.Contracts
{
    interface IFormatUserInput
    {
        string GetUserInput();

        int[] FormatInput(string userInput);
    }
}
