﻿using System;

namespace Adding2DArray.Contracts
{
    interface IArrayOperations
    { 
        int[,] Create2DArray(int[] arrayElements);

        Tuple<int[], int[]> SelectElementsToAdd(int[,] inputArray, int[] indexToStart);

        int[] AddSelectedElements(Tuple<int[], int[]> elementsToAdd);       
    }
}
