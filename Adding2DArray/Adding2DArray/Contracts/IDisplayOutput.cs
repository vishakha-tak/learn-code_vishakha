﻿using System;

namespace Adding2DArray.Contracts
{
    interface IDisplayOutput
    {
        void DisplayResult(int[] arrayOfSum);
    }
}
