import java.util.*;
import java.io.*;
 
class Statistics 
{
   	static HashMap <String,String> peopleNameAndSportList = new HashMap<>();
    static HashMap <String, Integer> allSportPopularityList = new HashMap<>();
	static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));  
	static int maximumPopularity = 0;
 
	public static void main (String args[]) throws IOException
	{
		fillingDataInPeopleNameAndSportList();
	   	findingPopularityOfAllSport();
	   	String mostFamousSport = findingMostPopularSport();
		showSurveyResult(mostFamousSport);
    	}
 
	public static void fillingDataInPeopleNameAndSportList() throws IOException
	{
		int numberOfEntries = Integer.parseInt(bufferedReader.readLine());
	    	for(int record=0; record<numberOfEntries; record++)
	    	{
    	   		String inputLine = bufferedReader.readLine();
			if(IsInputValid(numberOfEntries))
			{
    	   			String[] nameandfavouritesporttupple = inputLine.trim().split(" ");
				if(IsNameValid(nameandfavouritesporttupple[0] , nameandfavouritesporttupple[1]))
				{
    	   				if(!peopleNameAndSportList.containsKey(nameandfavouritesporttupple[0]))
    	       					peopleNameAndSportList.put(nameandfavouritesporttupple[0],nameandfavouritesporttupple[1]);
				}
			}
	    	}
	}
 
	public static boolean IsInputValid(int numberOfEntries)
	{
		if(numberOfEntries >= 1 && numberOfEntries <= 100000)
			return true;
		else
			return false;
 	}
 
	public static boolean IsNameValid(String peopleName, String sportsName)
	{
		if((peopleName.length() >= 1 && peopleName.length() <= 10) && (sportsName.length() >= 1 && sportsName.length() <= 10))
			return true;
		else
			return false;
 	}
 
	public static void findingPopularityOfAllSport()
	{
		for(String sport : peopleNameAndSportList.values())
       		{
    	   		if(!allSportPopularityList.containsKey(sport))
    	   			allSportPopularityList.put(sport,1);
    	   		else
    	   			allSportPopularityList.put(sport,(allSportPopularityList.get(sport)+1));
        	}
	}
 
	public static String findingMostPopularSport()
	{
		String mostPopularSport = " ";
        	for(Map.Entry<String,Integer> sportPopularityEntry : allSportPopularityList.entrySet())
        	{
    	   		String sport = sportPopularityEntry.getKey();
    	   		Integer popularity = sportPopularityEntry.getValue();
    	   		if(maximumPopularity < popularity)
    	   		{
    	       			maximumPopularity = popularity;
    	       			mostPopularSport = sport;
    	   		}
       		}
		return mostPopularSport;
	}
 
	public static void showSurveyResult(String mostPopularSport)
	{
		System.out.println(mostPopularSport);
		
    		if(allSportPopularityList.containsKey("football"))
    	   		System.out.println(allSportPopularityList.get("football"));
    		else
    	   		System.out.println("0");
	}
}