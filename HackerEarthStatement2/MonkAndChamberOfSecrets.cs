using System;
using System.Collections.Generic;
using System.Linq;

namespace MonkAndChamberOfSecrets
{
    public class Spider
    {
        private int power;
        private int position;

        public Spider(int pow, int pos)
        {
            this.power = pow;
            this.position = pos;
        }

        public int GetPosition()
        {
            return position;
        }

        public int GetPower()
        {
            return power;
        }

        public void SetPower(int power)
        {
            this.power = power;
        }
    }

    public class Monk
    {
        public static void LoadSpidersInQueue(int totalNumberOfSpiders, int numberOfSelectedSpiders, Queue<Spider> inputQueue)
        {
            int count = 1;

            String[] Line = Console.ReadLine().Split(' ');
            int[] numbers = Array.ConvertAll(Line, int.Parse);
            foreach (var inputPower in numbers)
            {
                Spider spiderObject = new Spider(inputPower, count++);

                inputQueue.Enqueue(spiderObject);
            }
        }

        public static int GetMaxValueSpiderIndex(Queue<Spider> inputQueue, int numberOfSelectedSpider)
        {
            numberOfSelectedSpider = numberOfSelectedSpider < inputQueue.Count ? numberOfSelectedSpider : inputQueue.Count;
            int powerOfMaximumValueSpider = Int32.MinValue;
            int positionOfMaximumValueSpider = Int32.MinValue;
            for (int index = 0; index < numberOfSelectedSpider; index++)
            {
                Spider spider = inputQueue.ElementAt(index);
                if (spider.GetPower() > powerOfMaximumValueSpider)
                {
                    positionOfMaximumValueSpider = spider.GetPosition();
                    powerOfMaximumValueSpider = spider.GetPower();
                }
            }
            return positionOfMaximumValueSpider;
        }

        public static void RemoveMaxValueSpider(Queue<Spider> inputQueue, int index, int numberOfSelectedSpider)
        {
            numberOfSelectedSpider = numberOfSelectedSpider < inputQueue.Count ? numberOfSelectedSpider : inputQueue.Count;
            for (int count = 0; count < numberOfSelectedSpider; count++)
            {
                Spider spider = inputQueue.Dequeue();
                if (spider.GetPosition() != index)
                {
                    if (spider.GetPower() == 0)
                    {
                        spider.SetPower(0);
                    }
                    else
                    {
                        spider.SetPower(spider.GetPower() - 1);
                    }
                    inputQueue.Enqueue(spider);
                }
            }
        }

        public static bool NumberOfIterationsValidations(int input, int minimumValue, int maximumValue)
        {
            if (minimumValue < input & input <= maximumValue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool NumberOfSpidersValidations(int input, int minimumValue, int numberOfIterations)
        {
            if (minimumValue < input & input <= numberOfIterations * numberOfIterations)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class Program
    {
        static Queue<Spider> inputQueue = new Queue<Spider>();

        const int minimumSpiders = 1;
        const int minimumNumberOfIterations = 1;
        const int maximumNumberOfIterations = 316;

        static void Main(string[] args)
        {
            var input = Console.ReadLine().Trim().Split(' ').Select(int.Parse).ToArray();
            int totalNumberOfSpiders = input[0];
            int numberOfSelectedSpider = input[1];
            int maxValueSpiderPosition;
            if (Monk.NumberOfIterationsValidations(input[1], minimumNumberOfIterations, maximumNumberOfIterations)
                && Monk.NumberOfSpidersValidations(input[0], minimumSpiders, input[1]))
            {
                Monk.LoadSpidersInQueue(totalNumberOfSpiders, numberOfSelectedSpider, inputQueue);
                for (int index = 0; index < numberOfSelectedSpider; index++)
                {
                    maxValueSpiderPosition = Monk.GetMaxValueSpiderIndex(inputQueue, numberOfSelectedSpider);
                    Monk.RemoveMaxValueSpider(inputQueue, maxValueSpiderPosition, numberOfSelectedSpider);
                    Console.Write(maxValueSpiderPosition + " ");
                }
            }
        }
    }
}