import { Component, OnInit, Injectable } from '@angular/core';
import { Asset } from '../../models/asset';
import { FormGroup, FormControl, Validator, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AssetAssignmentService } from '../../services/asset-assignment.service';
import { AssetService } from '../../services/asset.service';
import { AssetTypeService } from '../../services/asset-type.service';
import { EmployeeService } from '../../services/employee.service';
import { Router } from '@angular/router';
import { AssetAssignment } from '../../models/assetAssignment';
import { Employee} from '../../models/employee.model';
import { AssignmentLog } from '../../models/assignmentLog.model';
import{ AssetType } from '../../models/assettype.model';

@Component({
  selector: 'app-asset-assignment',
  templateUrl: './asset-assignment.component.html',
  styleUrls: ['./asset-assignment.component.css']
})
export class AssetAssignmentComponent implements OnInit {

  //Array Objects
  assetAssignmentList: AssetAssignment[] = Array<AssetAssignment>();
  assetList: Asset[] = Array<Asset>();
  empList: Employee[] =  Array<Employee>();
  assetTypeList: AssetType[] = Array<AssetType>();  
  assetAssignmentLog: AssignmentLog[] = Array<AssignmentLog>();
  EmpAssetList: AssetAssignment[] = Array<AssetAssignment>();
  
  assetAssignment: AssetAssignment;
  isAssetDeleted: boolean= false;
  isAssetAssignmentDeleted: boolean = false;
  isFormOn: boolean = false;
  isEmpFormOn: boolean = false;
  isAssignFormOn: boolean = false;
  assetAssignmentHistory: string = "none";
  buttonName: string = "Assign Asset";
  formTitle: string = "Assigning Asset";
  currentEmployee: Employee;
  currentEmployeeName: string;
  
  //Form control variables
  form: FormGroup;
  EmpId= new FormControl('', Validators.required);
  AssetTypeId= new FormControl('', Validators.required);
  AssetId= new FormControl('', Validators.required);
  Duration= new FormControl();


  constructor(private http: HttpClient,
    private assetAssignmentService: AssetAssignmentService,
    private assetService: AssetService,
    private assetTypeService: AssetTypeService,
    private employeeService: EmployeeService,
    private router: Router) {

  }

  ngOnInit() {
    this.isFormOn = true;
    this.isEmpFormOn = false;
    this.isAssignFormOn = false;

    this.getAssetAssignments();

    this.getAvailableAssets();

    this.getEmployees();

    this.getAssetTypes();    
    
  }

  /**
   * Method to get all Asset Assignments
   */
  getAssetAssignments(){
    this.assetAssignmentService.GetAssetAssignment().subscribe((Response: AssetAssignment[] ) => {
      this.assetAssignmentList = Response;
    },(error: any)=>{
      if (error.status == 404) {
        alert('No Asset Assignment found.');
      }
      else{
        alert(error.message);
      }
    });
  }

  /**
   * Method to get all Available Assets
   */
  getAvailableAssets(){
    this.assetService.GetAvailableAssets().subscribe((Response: any) => {
      this.assetList = Response;
      this.formTitle = "Assign Asset";
    },(error: any)=>{
      alert(error.message);
    });
  }

  /**
   * Method to get all Employees from Active Directory
   */
  getEmployees(){
    this.employeeService.getEmployees().subscribe((Response: any) => {
      this.empList = Response;
    },(error: any)=>{
      alert(error.message);
    });
  }

  /**
   * Method to get all Asset Types
   */
  getAssetTypes(){
    this.assetTypeService.GetAssetType().subscribe((Response: AssetType[]) => {
      this.assetTypeList = Response;
    },(error: any)=>{
      alert(error.message);
    });
  }

  /**
   * Method to set asset Visibility.
   */
  AssignAssetsVisibility() {
    this.isFormOn = false;
    this.isAssignFormOn = true;
    this.isEmpFormOn = false;
    this.assetList = null;
  }

  /**
   * Method for Adding New Assets.
   * @param obj : object
   * @param btnName : Name of button
   */
  assignAsset(obj: any, btnName: any) {
    this.assetAssignment = ({
      Emp_Id: obj.value.EmpId,
      Asset_Id: obj.value.AssetId,
      Duration: obj.value.Duration
    });
    this.assetAssignmentService.AssignAsset(this.assetAssignment).subscribe((Response: any) => {
      this.assetList = Response;
     this.getAvailableAssets();
      this.isFormOn = true;
      this.isEmpFormOn = false;
      this.isAssignFormOn = false;
      alert("Asset assignment done successfully");
    },(error: any)=>{
      alert(error.message);
    });
  }

  /**
   * Method for Reverting Assignment.
   * @param id :Id of assignment
   * @param employeeId : Employee Id
   */
  revertAssignment(id: any, employeeId: any) {
    if (confirm("Are you sure, you want to revert assignment?")) {
      this.assetAssignmentService.DeleteAssetAssignment(id).subscribe((Response: any) => {
        this.isAssetAssignmentDeleted = Response;
        alert("Asset assignment reverted successfully");
        if (this.isAssetAssignmentDeleted == true) {
          this.assetAssignmentService.GetEmployeeAssetsByID(employeeId)
            .subscribe((Response: any) => {
              this.EmpAssetList = Response;
            }, (error: any) => {
              this.EmpAssetList = null;
              if (error != null) {
                this.assetAssignmentService.GetAssetAssignment().subscribe((Response: any) => {
                  this.assetAssignmentList = Response;
                });
                this.isEmpFormOn = false;
                this.isFormOn = true;
              }
            });
        }
      });
    }
  }

  /**
   * Method for Deleting Assets.
   * @param id : Asset Id
   */
  deleteAsset(id: any) {
    if (confirm("Are you sure, you want to delete?")) {
      this.assetService.DeleteAsset(id).subscribe((Response: any) => {
        this.isAssetDeleted = Response;
        alert("Asset deleted successfully");
        this.getAvailableAssets();
      },(error: any)=>{
        alert(error.message);
      });
    }    
  }

  /**
   * Method to get Employee Detail.
   * @param employeeId : Id of employee
   */
  getEmployeeAssetAssignmentDetails(employeeId: any) {
    this.isAssignFormOn = false;
    this.assetAssignmentService.GetEmployeeAssetsByID(employeeId)
      .subscribe((Response: any) => {
        this.EmpAssetList = Response;
        this.isFormOn = false;
        this.isEmpFormOn = true;

      }, (error: any) => {
        if (error.status == 404) {
          alert('No Asset Assignment done.');
          this.isFormOn = true;
        }
      });
  }

  /**
   * Method to cancel/close form.
   */
  cancelform() {
    this.ngOnInit();
    this.buttonName = "Add Asset";
    this.isFormOn = true;
    this.isAssignFormOn = false;
    this.isEmpFormOn = false;
  }

  /**
   * Method to get details on click of History button.
   * @param employeeId : EmployeeId
   */
  onHistoryClick(employeeId: any) {
    this.assetAssignmentLog = null;
    this.assetAssignmentService.GetLogsByEmployeeId(employeeId).subscribe((Response: any) => {
      this.assetAssignmentLog = Response;
    },(error: any)=>{
      alert(error.message);
    });
    this.currentEmployee = this.filterEmployeee(employeeId);
    this.currentEmployeeName = this.currentEmployee[0].EmpName;
  }

  /**
   * Methods for filtering the assets.
   * @param assetId :Asset to filter.
   */
  filterAssets(assetId: any) {
    this.assetList = this.assetList.filter(x => x.AssetTypeId == assetId);
  }

  /**
   * Method for filtering Employee.
   * @param employeeId : Employee to filter
   */
  filterEmployeee(employeeId: any): Employee[] {
    return this.empList.filter(x => x.EmpId == employeeId);
  }
}