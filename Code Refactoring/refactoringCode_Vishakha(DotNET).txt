using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Schilling.ServiceHub.Models.Api;
using Schilling.ServiceHub.Models.Utility;
using Swashbuckle.SwaggerGen.Annotations;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Schilling.ServiceHub.Web.Api._beta.Pdf
{
    /// <summary>
    /// controller to provide PDF generation service (forwards to internal web app)
    /// </summary>
    [Authorize]
    [Route("beta/pdf")]
    [SwaggerOperationFilter(typeof(CategorizeFilter))]
    public class PdfServiceController: Controller
    {
        const string ByteArrayContentType = "application/octet-stream";

        private ILogger _logger;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="logger"></param>
        public PdfServiceController(ILogger<PdfServiceController> logger) {

            _logger = logger;

        }

        /// <summary>
        /// method to return PDF file, given html content
        /// </summary>
        /// <param name="htmlModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult<byte[]> PostPdfStream([FromBody]StringModel htmlModel)
        {
            var errorResult = new ApiResult<byte[]>();
            var htmlContent = htmlModel.StringValue;

            _logger.LogInformation("PostPdfStream method starting...");
            var isNull = htmlContent == null;
            _logger.LogInformation("htmlContent null? " + isNull);
            var headers = Request.Headers;
            _logger.LogInformation("Request headers = " + string.Join(";", headers));
            _logger.LogInformation("PostPdfStream htmlContent string = " + htmlContent);

            byte[] pdfBytes = null;

            try
            {
                var config = new HttpClientHandler
                {
                    UseDefaultCredentials = true
                };

                using (var httpClient = new HttpClient(config))
                {
                    var requestBody = JsonConvert.SerializeObject(htmlModel);
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));
                    var content = new StringContent(requestBody, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = new HttpResponseMessage();

                    response = httpClient.PostAsync(string.Format("http://labels-dev.dav1.net.fmcti.com/api/PdfService/"), content).GetAwaiter().GetResult();

                    if (response.IsSuccessStatusCode)
                    {
                        string result = string.Empty;
                        result = response.Content.ReadAsStringAsync().Result.Replace("\"", string.Empty);
                        pdfBytes = Convert.FromBase64String(result);
                        //return File(pdfBytes, ByteArrayContentType);
                        return ApiResult<byte[]>.FromModel(pdfBytes);
                    }
                    else
                    {
                        errorResult.Error = "Error returned from PDF API, status code = " +  response.StatusCode.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message + " - " + ex.InnerException?.Message;
                errorResult.Error = message;
                return errorResult;
            }
            return errorResult;
        }

    }


}
