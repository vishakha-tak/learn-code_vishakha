﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MonkAndChamberOfSecrets
{
    public class Spider
    {
        public int power;
        public int position;
        public Spider(int pow, int pos)
        {
            power = pow;
            position = pos;
        }
        public int GetPosition()
        {
            return position;
        }

        public int GetPower()
        {
            return power;
        }

        public void SetPower(int power)
        {
            this.power = power;
        }
    }

    public class Program
    {
        static Queue<Spider> inputQueue;
        static List<Spider> dqueuedSpiders = new List<Spider>();

        static void Main(string[] args)
        {
            var input = Console.ReadLine().Trim().Split(' ').Select(int.Parse).ToArray();
            int numberOfSelectedSpider = input[1], count = 0;
            int maxValueSpiderPosition;

            inputQueue = new Queue<Spider>(Console.ReadLine().Trim().Split(' ').Select(spiderObj => new Spider(int.Parse(spiderObj), ++count)));

            for (int i = 0; i < numberOfSelectedSpider; i++)
            {
                maxValueSpiderPosition = GetMaxValueSpiderIndex(numberOfSelectedSpider);
                removeMaxValue(maxValueSpiderPosition, numberOfSelectedSpider);
                Console.Write(maxValueSpiderPosition + " ");
            }
        }

        public static int GetMaxValueSpiderIndex(int numberOfSelectedSpider)
        {
            numberOfSelectedSpider = numberOfSelectedSpider < inputQueue.Count ? numberOfSelectedSpider : inputQueue.Count;
            int powerOfMaximumValueSpider = Int32.MinValue;
            int positionOfMaximumValueSpider = Int32.MinValue;
            for (int index = 0; index < numberOfSelectedSpider; index++)
            {
                Spider spider = inputQueue.ElementAt(index);
                if (spider.GetPower() > powerOfMaximumValueSpider)
                {
                    positionOfMaximumValueSpider = spider.GetPosition();
                    powerOfMaximumValueSpider = spider.GetPower();
                }
            }
            return positionOfMaximumValueSpider;
        }

        public static void removeMaxValue(int index, int numberOfSelectedSpider)
        {
            numberOfSelectedSpider = numberOfSelectedSpider < inputQueue.Count ? numberOfSelectedSpider : inputQueue.Count;
            for (int count = 0; count < numberOfSelectedSpider; count++)
            {
                Spider spider = inputQueue.Dequeue();
                if (spider.GetPosition() != index)
                {
                    if (spider.GetPower() == 0)
                    {
                        spider.SetPower(0);
                    }
                    else
                    {
                        spider.SetPower(spider.GetPower() - 1);
                    }
                    inputQueue.Enqueue(spider);
                }
            }
        }
    }
}
