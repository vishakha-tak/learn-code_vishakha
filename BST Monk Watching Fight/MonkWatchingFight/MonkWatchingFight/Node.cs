﻿using System;

namespace MonkWatchingFight
{
    public class Node
    {
        private int _data;
        private Node _leftNode;
        private Node _rightNode;
        public Node()
        {
            _data = 0;
            _leftNode = null;
            _rightNode = null;
        }
        public Node(int data, Node leftNode, Node rightNode)
        {
            _data = data;
            _leftNode = leftNode;
            _rightNode = rightNode;
        }

        public int Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }
        public Node LeftNode
        {
            get
            {
                return _leftNode;
            }
            set
            {
                _leftNode = value;
            }
        }
        public Node RightNode
        {
            get
            {
                return _rightNode;
            }
            set
            {
                _rightNode = value;
            }
        }
    }
}
