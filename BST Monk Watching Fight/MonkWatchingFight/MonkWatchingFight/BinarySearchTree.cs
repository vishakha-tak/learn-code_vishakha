﻿using System;

namespace MonkWatchingFight
{
    public class BinarySearchTree
    {
        //DI 
        private Node root;
        public Node getRootNode()
        {
            return root;
        }
        public void setRootNode(Node root)
        {
            this.root = root;
        }
        public void createBST(int value)
        {
            root = AddNode(root, value);
        }

        public Node AddNode(Node root, int value)
        {
            if (root == null)
            {
                root = new Node(value, null, null);
                return root;

            }
            if (root.Data < value)
            {
                root.RightNode = AddNode(root.RightNode, value);
            }
            else
            {
                root.LeftNode = AddNode(root.LeftNode, value);
            }
            return root;
        }

        public int GetHeightOfBST(Node node)
        {
            if (node == null)
                return 0;
            else
                return 1 + Max(GetHeightOfBST(node.LeftNode), GetHeightOfBST(node.RightNode));
        }
        public int Max(int leftSideHeight, int rightSideHeight)
        {
            return leftSideHeight > rightSideHeight ? leftSideHeight : rightSideHeight;
        }
        public bool isValidNumberOfElements(int numberOfElements)
        {
            return (numberOfElements >= 1 && numberOfElements <= (1000));
        }
    }
}
