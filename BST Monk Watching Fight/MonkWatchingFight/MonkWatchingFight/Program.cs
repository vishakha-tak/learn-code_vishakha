﻿using System;

namespace MonkWatchingFight
{
    class Program
    {
        public static void Main(string[] args)
        {
            BinarySearchTree binarySearchTree = new BinarySearchTree();

            int numberOfElements = int.Parse(Console.ReadLine());
            if (binarySearchTree.isValidNumberOfElements(numberOfElements))
            {
                string[] input = Console.ReadLine().Split(' ');
                
                for (int nodeNumber = 0; nodeNumber < numberOfElements; nodeNumber++)
                {
                    binarySearchTree.createBST(int.Parse(input[nodeNumber]));
                }
                Console.WriteLine(binarySearchTree.GetHeightOfBST(binarySearchTree.getRootNode()));
                Console.ReadKey();
            }
        }
    }   
}
