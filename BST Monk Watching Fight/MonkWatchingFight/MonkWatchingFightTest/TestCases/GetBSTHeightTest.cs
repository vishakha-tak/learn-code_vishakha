﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MonkWatchingFight;


namespace MonkWatchingFightTest
{
    [TestClass]
    public class GetBSTHeightTest
    {
        BinarySearchTree binarySearchTree = new BinarySearchTree();

        [TestMethod]
        public void GetHeightWhenOnlyOneNodeTreeTest()
        {
            binarySearchTree.createBST(5);
            Assert.AreEqual(binarySearchTree.GetHeightOfBST(binarySearchTree.getRootNode()), 1);
        }

        [TestMethod]
        public void GetHeightWhenTwoNodesTreeTest()
        {
            binarySearchTree.createBST(5);
            binarySearchTree.createBST(1);
            Assert.AreEqual(binarySearchTree.GetHeightOfBST(binarySearchTree.getRootNode()), 2);
        }       

        [TestMethod]
        public void GetHeightWithEqualHeightTreeTest()
        {
            binarySearchTree.createBST(5);
            binarySearchTree.createBST(10);
            binarySearchTree.createBST(1);
            Assert.AreEqual(binarySearchTree.GetHeightOfBST(binarySearchTree.getRootNode()), 2);
        }

        [TestMethod]
        public void GetHeightForRightTreeTest()
        {
            binarySearchTree.createBST(5);
            binarySearchTree.createBST(8);
            binarySearchTree.createBST(13);
            binarySearchTree.createBST(20);
            binarySearchTree.createBST(25);
            binarySearchTree.createBST(30);
            binarySearchTree.createBST(35);
            Assert.AreEqual(binarySearchTree.GetHeightOfBST(binarySearchTree.getRootNode()), 7);
        }

        [TestMethod]
        public void GetHeightForLeftTreeTest()
        {
            binarySearchTree.createBST(70);
            binarySearchTree.createBST(60);
            binarySearchTree.createBST(55);
            binarySearchTree.createBST(20);
            binarySearchTree.createBST(10);
            binarySearchTree.createBST(5);
            Assert.AreEqual(binarySearchTree.GetHeightOfBST(binarySearchTree.getRootNode()), 6);
        }
    }
}
