﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MonkWatchingFight;

namespace MonkWatchingFightTest
{
    [TestClass]
    public class CreateBinarySearchTreeTest
    {
        BinarySearchTree binarySearchTree = new BinarySearchTree();

        [TestMethod]
        public void CreateBSTWithOneNodeTest()
        {
            binarySearchTree.createBST(7);
            Assert.AreEqual(binarySearchTree.getRootNode().Data, 7);
        }
        [TestMethod]
        public void CreateBSTWithTwoNodesTest()
        {           
            binarySearchTree.createBST(7);
            binarySearchTree.createBST(3);
            Assert.AreEqual(binarySearchTree.getRootNode().LeftNode.Data, 3);
        }

        [TestMethod]
        public void CreateBSTWithEqualHeightTest()
        {
            BinarySearchTree binarySearchTree = new BinarySearchTree();
            binarySearchTree.createBST(7);
            binarySearchTree.createBST(10);
            binarySearchTree.createBST(3);
            Assert.AreEqual(binarySearchTree.getRootNode().LeftNode.Data, 3);
        }

        [TestMethod]
        public void CreateBSTWithOnlyRightTreeTest()
        {
            BinarySearchTree binarySearchTree = new BinarySearchTree();
            binarySearchTree.createBST(7);
            binarySearchTree.createBST(10);
            binarySearchTree.createBST(15);
            binarySearchTree.createBST(20);
            binarySearchTree.createBST(25);
            binarySearchTree.createBST(30);
            binarySearchTree.createBST(35);
            Assert.AreEqual(binarySearchTree.getRootNode().RightNode.RightNode.RightNode.RightNode.RightNode.RightNode.Data, 35);
        }

        [TestMethod]
        public void CreateBSTWithOnlyLeftTreeTest()
        {
            BinarySearchTree binarySearchTree = new BinarySearchTree();
            binarySearchTree.createBST(70);
            binarySearchTree.createBST(65);
            binarySearchTree.createBST(60);
            binarySearchTree.createBST(20);
            binarySearchTree.createBST(15);
            binarySearchTree.createBST(10);
            Assert.AreEqual(binarySearchTree.getRootNode().LeftNode.LeftNode.LeftNode.LeftNode.LeftNode.Data, 10);
        }       
    }
}
