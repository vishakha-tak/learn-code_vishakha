﻿using System;

namespace LearnAndCode
{
    class Program
    {
        public static void Main(string[] args)
        {
            BinarySearchTre binarySearchTree = new BinarySearchTre();

            int numberOfElements = int.Parse(Console.ReadLine());
            if (binarySearchTree.isValidNumberOfElements(numberOfElements))
            {
                string[] input = Console.ReadLine().Split(' ');
                
                for (int nodeNumber = 0; nodeNumber < numberOfElements; nodeNumber++)
                {

                    binarySearchTree.createBST(int.Parse(input[nodeNumber]));
                }
                Console.WriteLine(binarySearchTree.Height(binarySearchTree.getRootNode()));
            }
        }
    }
    public class Node
    {
        private int _data;
        private Node _leftNode;
        private Node _rightNode;
        public Node()
        {
            _data = 0;
            _leftNode = null;
            _rightNode = null;
        }
        public Node(int data, Node leftNode, Node rightNode)
        {
            _data = data;
            _leftNode = leftNode;
            _rightNode = rightNode;
        }
        
        public int Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }
        public Node LeftNode
        {
            get
            {
                return _leftNode;
            }
            set
            {
                _leftNode = value;
            }
        }
        public Node RightNode
        {
            get
            {
                return _rightNode;
            }
            set
            {
                _rightNode = value;
            }
        }
    }
    public class BinarySearchTre
    {
        private Node root;

        public Node getRootNode()
        {
            return root;
        }

        public void setRootNode(Node root)
        {
            this.root = root;
        }
        public void createBST(int value)
        {
            root = AddNode(root, value);
        }

        public Node AddNode(Node root, int value)
        {
            if (root == null)
            {
                root = new Node(value, null, null);
                return root;

            }
            if (root.Data < value)
            {
                root.RightNode = AddNode(root.RightNode, value);
            }
            else
            {
                root.LeftNode = AddNode(root.LeftNode, value);
            }
            return root;
        }

        public int Height(Node node)
        {
            if (node == null)
                return 0;
            else
                return 1 + Max(Height(node.LeftNode), Height(node.RightNode));
        }

        public int Max(int leftSideHeight, int rightSideHeight)
        {
            return leftSideHeight > rightSideHeight ? leftSideHeight : rightSideHeight;
        }
        public bool isValidNumberOfElements(int numberOfElements)
        {
            return (numberOfElements >= 1 && numberOfElements <= (1000));
        }
    }   
}
